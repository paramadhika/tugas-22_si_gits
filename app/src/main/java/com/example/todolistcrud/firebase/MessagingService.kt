package com.example.todolistcrud.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.PendingIntent.FLAG_ONE_SHOT
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.todolistcrud.DetailActivity
import com.example.todolistcrud.R
import com.example.todolistcrud.login_register.LoginActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

const val channelId = "notification_channel"
const val channelName = "com.example.todolistcrud"

class MessagingService : FirebaseMessagingService() {

    lateinit var builder: NotificationCompat.Builder

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "The Token Refreshed : $token")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        sendNotification(remoteMessage)

    }

    fun sendNotification(remoteMessage: RemoteMessage){

        val intent : Intent

        if (remoteMessage.data["title"] == "Completed To Do") {
            intent = Intent(this, DetailActivity::class.java)
        }
        else {
            intent = Intent(this, LoginActivity::class.java)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, FLAG_ONE_SHOT)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


        if (remoteMessage.data.isNullOrEmpty()){
            Log.d(TAG, "data message 1 : ${remoteMessage.data}")
            builder = NotificationCompat.Builder(applicationContext, channelId)
                .setSmallIcon(R.drawable.uncomplete_todo)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(1000,1000))
                .setOnlyAlertOnce(true)
                .setContentTitle(remoteMessage.notification?.title)
                .setContentText(remoteMessage.notification?.body)
                .setContentIntent(pendingIntent)
        }
        else if (remoteMessage.data.isNotEmpty()){
            builder = NotificationCompat.Builder(applicationContext, channelId)
                .setSmallIcon(R.drawable.uncomplete_todo)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(1000,1000))
                .setOnlyAlertOnce(true)
                .setContentTitle(remoteMessage.data["title"])
                .setContentText(remoteMessage.data["message"])
                .setContentIntent(pendingIntent)
        }
        else{
            val data = JSONObject(remoteMessage.data.toString())
            builder = NotificationCompat.Builder(applicationContext, channelId)
                .setSmallIcon(R.drawable.uncomplete_todo)
                .setAutoCancel(true)
                .setVibrate(longArrayOf(1000,1000))
                .setOnlyAlertOnce(true)
                .setContentTitle("Testing Notifikasi dari backend")
                .setContentText(data.getString("message"))
                .setContentIntent(pendingIntent)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val notificationChannel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(notificationChannel)
        }

        notificationManager.notify(0, builder.build())

    }
}