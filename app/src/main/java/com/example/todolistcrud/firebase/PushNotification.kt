package com.example.todolistcrud.firebase

data class PushNotification(
    val data: NotificationData,
    val to: String
)