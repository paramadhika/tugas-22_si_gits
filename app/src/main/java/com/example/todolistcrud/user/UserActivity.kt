package com.example.todolistcrud.user

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.todolistcrud.MainActivity
import com.example.todolistcrud.R
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.Constant.Companion.PREF_ID_USER
import com.example.todolistcrud.helper.Constant.Companion.PREF_PHOTO_PROFILE
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.login_register.LoginActivity
import com.example.todolistcrud.model.ResponseGetTodoList
import com.example.todolistcrud.model.ResponseUser
import kotlinx.android.synthetic.main.activity_user.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private val id by lazy { intent.getStringExtra("idUser") }
    private lateinit var totalCompletedTodoView : TextView
    private lateinit var totalUncompletedTodoView : TextView
    private lateinit var totalTodoView : TextView
    private lateinit var firstName : String
    private lateinit var lastName : String
    private lateinit var emailUser : TextView
    private lateinit var username : TextView
    private lateinit var pass :String
    private lateinit var deviceID :String
    private lateinit var photo : ImageView
    var imgPath: String? = null
    lateinit var sharedpref: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedpref = PreferencesHelper(this)

        setContentView(R.layout.activity_user)
        setUpView()
        totalTodo()
        totalUncompletedTodo()
        totalCompletedTodo()
        onClick()
        editListener()
        setLogoutListener()
        getUserIdentity()
    }

    private fun setLogoutListener() {
        LogOutBtn.setOnClickListener {
            sharedpref.clear()
            Toast.makeText(
                applicationContext,
                "Berhasil Logout!",
                Toast.LENGTH_SHORT
            ).show()
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun onClick() {
        completedTodoList.setOnClickListener {
            sharedpref.put(Constant.PREF_TYPE_FUNCTION, "getCompleted")
            val intent = Intent(this, MainActivity::class.java)
                .putExtra("username", username.text.toString())
            startActivity(intent)
        }

        uncompletedTodoList.setOnClickListener {
            sharedpref.put(Constant.PREF_TYPE_FUNCTION, "getUncompleted")
            val intent = Intent(this, MainActivity::class.java)
                .putExtra("username", username.text.toString())
            startActivity(intent)
        }

        AllTodoList.setOnClickListener {
            sharedpref.put(Constant.PREF_TYPE_FUNCTION, "getTodoList")
            val intent = Intent(this, MainActivity::class.java)
                .putExtra("username", username.text.toString())
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        getUserIdentity()
        if (imgPath == null && sharedpref.geString(PREF_PHOTO_PROFILE) == ""){
            photo.setImageResource(R.drawable.ic_launcher_foreground)
        }
        else{
            Glide.with(this).load(sharedpref.geString(PREF_PHOTO_PROFILE)).into(photo)
        }
    }

    private fun setUpView() {
        totalCompletedTodoView = findViewById(R.id.totalTodoCompleted)
        totalUncompletedTodoView = findViewById(R.id.totalTodoUncompleted)
        totalTodoView = findViewById(R.id.TotalTodo)
        emailUser = findViewById(R.id.emailUser)
        username = findViewById(R.id.profil_username)
        photo = findViewById(R.id.foto_profil)
    }

    private fun editListener() {
        update_user.setOnClickListener {
            val intent = Intent(this, EditUserActivity::class.java)
            intent.putExtra("fn", firstName)
            intent.putExtra("ln", lastName)
            intent.putExtra("email", emailUser.text.toString())
            intent.putExtra("pass", pass)
            intent.putExtra("image", imgPath)
            intent.putExtra("deviceID", deviceID)
            startActivity(intent)
            onStart()
        }
    }

    private fun getUserIdentity() {
        api.getUserIdentity("User.php", "ListUser")
            .enqueue(object : Callback<ResponseUser>{
                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<ResponseUser>,
                    response: Response<ResponseUser>?
                ) {
                    for(item in response!!.body()!!.data!!){
                        if (item!!.id == sharedpref.geString(PREF_ID_USER)){
                            firstName = item.firstname.toString()
                            lastName = item.lastname.toString()
                            emailUser.text = item.email.toString()
                            username.text = item.firstname.toString() + " " + item.lastname.toString()
                            pass = item.password.toString()
                            imgPath = item.image.toString()
                            deviceID = item.device_id.toString()
                        }
                    }
                }
                override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "ERROR!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun totalTodo() {
        api.getTodoList("Todo.php", "getTodoList")
            .enqueue(object : Callback<ResponseGetTodoList>{
                override fun onResponse(
                    call: Call<ResponseGetTodoList>,
                    response: Response<ResponseGetTodoList>
                ) {
                    val total = response.body()!!.data
                    totalTodoView.text = total!!.size.toString()
                }

                override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                    totalTodoView.text = "0"
                }

            })
    }

    private fun totalUncompletedTodo() {
        api.getUncompletedTodo("Todo.php", "getUncompleted")
            .enqueue(object : Callback<ResponseGetTodoList>{
                override fun onResponse(
                    call: Call<ResponseGetTodoList>,
                    response: Response<ResponseGetTodoList>
                ) {
                    val total = response.body()!!.data
                    totalUncompletedTodoView.text = total!!.size.toString()
                }

                override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                    totalUncompletedTodoView.text = "0"
                }

            })
    }

    private fun totalCompletedTodo() {
        api.getCompletedTodo("Todo.php", "getCompleted")
            .enqueue(object : Callback<ResponseGetTodoList> {
                override fun onResponse(
                    call: Call<ResponseGetTodoList>,
                    response: Response<ResponseGetTodoList>
                ) {
                    val total = response.body()?.data
                    totalCompletedTodoView.text = total?.size.toString()
                }
                override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                    totalCompletedTodoView.text = "0"
                }

            })
    }
}