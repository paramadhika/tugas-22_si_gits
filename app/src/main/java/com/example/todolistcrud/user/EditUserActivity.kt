package com.example.todolistcrud.user

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.text.method.PasswordTransformationMethod
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.EditText
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.todolistcrud.R
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.Constant.Companion.PREF_ID_DEVICE
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.ResponseUser
import com.example.todolistcrud.model.SubmitTodo
import com.vincent.filepicker.Constant.*
import com.vincent.filepicker.activity.ImagePickActivity
import com.vincent.filepicker.filter.entity.ImageFile
import kotlinx.android.synthetic.main.activity_edit_user.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditUserActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private lateinit var setfirstname : EditText
    private lateinit var setlastname : EditText
    private lateinit var setemail : EditText
    private lateinit var setpass : EditText
    private lateinit var setPhotoBtn : ImageButton
    private lateinit var setImage : ImageView
    lateinit var body: MultipartBody.Part
    lateinit var sharedpref: PreferencesHelper
    lateinit var imagePath : String
    private lateinit var deviceID :String
    private var isBound : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_user)

        sharedpref = PreferencesHelper(this)

        deviceID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        setTempDataUser()
        setPhotoProfile()
        saveEditListener()
        setBackBtn()
    }

    override fun onStart() {
        super.onStart()
        setTempDataUser()
        setPhotoProfile()
        isAccBound()
    }

    @SuppressLint("SetTextI18n")
    private fun isAccBound() {
        if (sharedpref.geString(PREF_ID_DEVICE) == ""){
            bindInfo.visibility = GONE
            bindAccbtn.setOnClickListener {
                var alert = AlertDialog.Builder(this)
                alert.setTitle("Bind Account")
                alert.setMessage("Are You Sure?")
                checkBound(deviceID)
                alert.setPositiveButton("Yes") { dialog: DialogInterface?, which: Int ->
                    if (checkBound(deviceID)){
                        Toast.makeText(
                            applicationContext,
                            "This Device has bound to another account",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else{
                        sharedpref.put(PREF_ID_DEVICE, deviceID)
                        api.updateDeviceID("User.php",
                            "update_deviceId",
                            sharedpref.geString(Constant.PREF_ID_USER)!!.toInt(),
                            deviceID
                        ).enqueue(object : Callback<SubmitTodo>{
                            override fun onResponse(
                                call: Call<SubmitTodo>,
                                response: Response<SubmitTodo>
                            ) {
                                if (response.isSuccessful){
                                    Toast.makeText(
                                        applicationContext,
                                        response.body()!!.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    onStart()
                                }
                                else{
                                    Toast.makeText(
                                        applicationContext,
                                        response.body()!!.message,
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                            override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                                Toast.makeText(
                                    applicationContext,
                                    "ERROR : $t",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        })
                    }
                }
                alert.setNegativeButton("No") { dialog: DialogInterface?, which: Int ->
                    onStart()
                }
                alert.show()
                onStart()
            }
        }
        else{
            bindInfo.visibility = VISIBLE
            bindInfo.setText("Your Account has bound to\nthis device(${sharedpref.geString(Constant.PREF_ID_DEVICE)})")
            isAccbind.setText("Unbind account from this Device?")
            bindAccbtn.setText(" Unbind Now")
            bindAccbtn.setOnClickListener {
                var alert = AlertDialog.Builder(this)
                alert.setTitle("Unbind Account")
                alert.setMessage("Are You Sure?")
                alert.setPositiveButton("Yes") { dialog: DialogInterface?, which: Int ->
                    api.updateDeviceID("User.php",
                        "update_deviceId",
                        sharedpref.geString(Constant.PREF_ID_USER)!!.toInt(),
                        ""
                    ).enqueue(object : Callback<SubmitTodo>{
                        override fun onResponse(
                            call: Call<SubmitTodo>,
                            response: Response<SubmitTodo>
                        ) {
                            if (response.isSuccessful){
                                Toast.makeText(
                                    applicationContext,
                                    response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                sharedpref.put(Constant.PREF_ID_DEVICE, "")
                                isAccbind.setText("Bind your account to this device?")
                                bindAccbtn.setText(" Bind Now")
                                onStart()
                            }
                            else{
                                Toast.makeText(
                                    applicationContext,
                                    response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "ERROR : $t",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    })
                }
                alert.setNegativeButton("No") { dialog: DialogInterface?, which: Int ->
                    onStart()
                }
                alert.show()
            }
        }
    }

    private fun checkBound(deviceId : String): Boolean {
        api.getUserIdentity("User.php", "ListUser")
            .enqueue(object : Callback<ResponseUser>{
                override fun onResponse(
                    call: Call<ResponseUser>,
                    response: Response<ResponseUser>
                ) {
                    for(item in response.body()!!.data!!){
                        if (item!!.device_id == deviceId){
                            isBound = true
                        }
                    }
                }
                override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                }
            })
        return isBound
    }

    private fun setBackBtn() {
        back.setOnClickListener {
            val intent = Intent(this, UserActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        setImage = findViewById(R.id.set_foto_profil)


        if (requestCode == REQUEST_CODE_PICK_IMAGE && resultCode == Activity.RESULT_OK && data != null)
        {
            val pilihImg = data.getParcelableArrayListExtra<ImageFile>(RESULT_PICK_IMAGE)?.get(0)?.path
            val requestBody = RequestBody.create("multipart".toMediaTypeOrNull(), File(pilihImg))
            body = MultipartBody.Part.createFormData("file", File(pilihImg).toString(), requestBody)

            imagePath = pilihImg.toString()

            Glide.with(this).load(pilihImg).into(setImage)
            if (pilihImg != null) {
                sharedpref.put(Constant.PREF_PHOTO_PROFILE, pilihImg)
            }
        }
    }

    private fun setPhotoProfile() {
        setPhotoBtn = findViewById(R.id.change_photo)

        setPhotoBtn.setOnClickListener {
            if (EasyPermissions.hasPermissions(this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                val i = Intent(this, ImagePickActivity::class.java)
                    .putExtra(MAX_NUMBER, 1)
                startActivityForResult(i, REQUEST_CODE_PICK_IMAGE)
            }
            else{
                EasyPermissions.requestPermissions(this, "This application need your permission to acces your galery."
                , 991, android.Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        }
    }

    private fun saveEditListener() {
        saveEdit.setOnClickListener {
            intent.putExtra("fn", setfirstname.text.toString())
            intent.putExtra("ln", setlastname.text.toString())
            intent.putExtra("email", setemail.text.toString())
            intent.putExtra("pass", setpass.text.toString())
            if (setfirstname.text.toString() == "" || setlastname.text.toString() == "" ||
                setemail.text.toString() == "" || setpass.text.toString() == ""){
                Toast.makeText(this, "Harap Lengkapi Data!", Toast.LENGTH_LONG).show()
            }
            else{
                upload()
                api.updateUser("User.php", "updateUser",
                    sharedpref.geString(Constant.PREF_ID_USER)!!.toInt(),
                    setfirstname.text.toString(),
                    setlastname.text.toString(),
                    setemail.text.toString(),
                    setpass.text.toString(),
                    imagePath)
                    .enqueue(object : Callback<ResponseUser>{
                        override fun onResponse(
                            call: Call<ResponseUser>,
                            response: Response<ResponseUser>
                        ) {
                            if (response.isSuccessful){
                                Toast.makeText(
                                    applicationContext,
                                    "Data Berhasil Di Update!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else {
                                Toast.makeText(
                                    applicationContext,
                                    "Data Tidak Berhasil Di Update!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "ERROR!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    })
            }
        }
    }

    private fun upload() {

        api.uploadImage("User.php", "user_image", body)
            .enqueue(object : Callback<SubmitTodo>{
                override fun onResponse(call: Call<SubmitTodo>, response: Response<SubmitTodo>) {
                    Toast.makeText(
                        applicationContext,
                        response.body()!!.message,
                        Toast.LENGTH_SHORT
                    )
                }

                override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "ERROR : Upload to Server Failed! $t",
                        Toast.LENGTH_SHORT
                    ).show()

                    Toast.makeText(
                        applicationContext,
                        "ERROR : Upload to Server Failed! $t",
                        Toast.LENGTH_SHORT
                    ).show()

                    Toast.makeText(
                        applicationContext,
                        "ERROR : Upload to Server Failed! $t",
                        Toast.LENGTH_SHORT
                    ).show()

                    Toast.makeText(
                        applicationContext,
                        "ERROR : Upload to Server Failed! $t",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
    }

    private fun setTempDataUser() {
        setfirstname = findViewById(R.id.setfirstName)
        setlastname = findViewById(R.id.setlastName)
        setemail = findViewById(R.id.setEmail)
        setpass = findViewById(R.id.setPass)
        setImage = findViewById(R.id.set_foto_profil)

        val bundle = intent.extras
        setfirstname.setText(bundle!!.getString("fn"))
        setlastname.setText(bundle.getString("ln"))
        setemail.setText(bundle.getString("email"))
        setpass.setText(bundle.getString("pass"))
        setpass.transformationMethod = PasswordTransformationMethod.getInstance()

        if (sharedpref.geString(Constant.PREF_PHOTO_PROFILE) == "" && bundle.getString("image") == ""){
            setImage.setImageResource(R.drawable.ic_launcher_foreground)
        }
        else{
            Glide.with(this).load(sharedpref.geString(Constant.PREF_PHOTO_PROFILE)).into(setImage)
        }
    }
}