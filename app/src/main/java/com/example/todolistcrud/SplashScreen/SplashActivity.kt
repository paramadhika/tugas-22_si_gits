package com.example.todolistcrud.SplashScreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import com.example.todolistcrud.R
import com.example.todolistcrud.login_register.LoginActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val splashTime : Long = 3000

        YoYo.with(Techniques.FadeIn)
            .duration(2000)
            .playOn(titleSplash)

        animationSplash.animate()
            .translationY(-1600F)
            .setDuration(750)
            .setStartDelay(2350)

        titleSplash.animate()
            .scaleY(0F)
            .setDuration(250)
            .setStartDelay(2000)

        transitionScreen.animate()
            .translationY(-2250F)
            .setDuration(450)
            .setStartDelay(2500)

        Handler(Looper.myLooper()!!).postDelayed({
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideUp(this)
            finish()
        }, splashTime)
    }
}
