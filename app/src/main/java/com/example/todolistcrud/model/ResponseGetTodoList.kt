package com.example.todolistcrud.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGetTodoList(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null

) : Parcelable

@Parcelize
data class DataItem(

	@field:SerializedName("todo")
	val todo: String? = null,

	@field:SerializedName("date_finished")
	val dateFinished: String? = null,

	@field:SerializedName("date_created")
	val dateCreated: String? = null,

	@field:SerializedName("todoStatus")
	val todoStatus: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("message")
	val message : String? = null,

	@field:SerializedName("status")
	val status : Int? = null
) : Parcelable
