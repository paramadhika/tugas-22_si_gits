package com.example.todolistcrud.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseNotification (

    @field:SerializedName("data")
    val data: List<DataNotification?>? = null,

    @field:SerializedName("to")
    val to: String? = null

) : Parcelable

@Parcelize
data class DataNotification(

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("body")
    val body: String? = null

) : Parcelable