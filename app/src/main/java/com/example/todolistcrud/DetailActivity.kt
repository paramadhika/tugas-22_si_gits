package com.example.todolistcrud

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.example.todolistcrud.alarm.AlarmReceiver
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.ResponseGetTodoList
import com.example.todolistcrud.model.SubmitTodo
import com.google.android.material.textfield.TextInputLayout
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_detail.back
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.text.*

class DetailActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private val todo by lazy { intent.getStringExtra("todo") }
    private val id by lazy { intent.getStringExtra("id") }

    private lateinit var picker : MaterialTimePicker
    private lateinit var calendar: Calendar
    private lateinit var alarmManager: AlarmManager
    private lateinit var pendingIntent: PendingIntent

    private lateinit var setReminderTime : EditText
    private lateinit var setReminderBtn : Button
    private lateinit var resetAlarm : ImageView

    lateinit var data: String
    lateinit var sharedpref: PreferencesHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        sharedpref = PreferencesHelper(this)

        setUpView()
        setEditListerner()
        setCompletedListener()
        createNotificationChannel()
        setTimeReminder()
        setAlarm()
        resetTimeReminder()

        back.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        setUpView()
    }

    private fun resetTimeReminder() {
        resetAlarm = findViewById(R.id.resetReminder)
        resetAlarm.setOnClickListener {

            alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
            val intent = Intent(this@DetailActivity, AlarmReceiver::class.java)
                .let { intent ->  PendingIntent.getBroadcast(this, 0, intent, 0) }

            alarmManager.cancel(intent)

            Toast.makeText(this, "Reminder Task Cancelled", Toast.LENGTH_SHORT).show()
            ReminderInfo.setText("00 : 00 ")
            onStart()
        }
    }

    private fun setTimeReminder() {
        setReminderTime = findViewById(R.id.ReminderInfo)
        setReminderTime.setOnClickListener {
            setTimePicker()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setTimePicker() {

        picker = MaterialTimePicker.Builder()
            .setTitleText("Set Reminder Task")
            .setTimeFormat(TimeFormat.CLOCK_12H)
            .setHour(12)
            .setMinute(10)
            .build()

        picker.show(supportFragmentManager, "DetailActivity")

        picker.addOnPositiveButtonClickListener {

            if(picker.hour > 12){
                ReminderInfo.setText(
                    String.format("%02d", picker.hour - 12)
                            + " : "
                            + String.format("%02d", picker.minute)
                            + " AM"
                )
            }
            else{
                ReminderInfo.setText(
                    String.format("%02d", picker.hour)
                            + " : "
                            + String.format("%02d", picker.minute)
                            + " PM"
                )
            }
            calendar = Calendar.getInstance()
            calendar[Calendar.HOUR_OF_DAY] = picker.hour
            calendar[Calendar.MINUTE] = picker.minute
            calendar[Calendar.SECOND] = 0
            calendar[Calendar.MILLISECOND] = 0
            onStart()
        }
    }

    private fun setAlarm() {
        setReminderBtn = findViewById(R.id.setReminder)
        setReminderBtn.setOnClickListener {
            alarmManager = getSystemService(ALARM_SERVICE) as AlarmManager
            val intent = Intent(this, AlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)


            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntent

            )

            Toast.makeText(this, "Reminder have been set succesfuly", Toast.LENGTH_SHORT).show()
        }

    }

    private fun createNotificationChannel() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = "com.example.todolistcrud"
            val description = "Channel for Alarm Manager"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("reminder", name, importance)
            channel.description = description
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )

            notificationManager.createNotificationChannel(channel)
        }

    }

    private fun sendNotification() {
        api.pushNotification(
            "sendNotification.php",
            "pushNotification",
            "global",
        "You Have Completed Task : $todo",
        "Completed To Do")
            .enqueue(object : Callback<SubmitTodo>{
                override fun onResponse(call: Call<SubmitTodo>, response: Response<SubmitTodo>) {
                    Toast.makeText(
                        applicationContext,
                        response.body()?.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "Gagal Mengirim Notifikasi --> $t",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun setEditListerner() {
        updateTodo.setOnClickListener {
            api.updateToDo("Todo.php", "updateTodo", id!!.toInt(), todoactivity.text.toString(), 0 )
                .enqueue(object : Callback<SubmitTodo>{
                    override fun onResponse(
                        call: Call<SubmitTodo>?,
                        response: Response<SubmitTodo>?
                    ) {
                        if (response!!.isSuccessful){
                            Toast.makeText(
                                applicationContext,
                                response!!.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else{
                            Toast.makeText(
                                applicationContext,
                                response.body()!!.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                    override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Data berhasil di Update!",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }
                })
        }
    }

    private fun setUpView() {

        val todoactivity : EditText = findViewById(R.id.todoactivity)
        val todoStatus : EditText = findViewById(R.id.todostatus)
        val dateCreated : EditText = findViewById(R.id.dateStart)
        val dateFinished : EditText = findViewById(R.id.dateFinish)

        api.getTodoList("Todo.php", "getTodoList")
            .enqueue(object : Callback<ResponseGetTodoList>{
                override fun onResponse(
                    call: Call<ResponseGetTodoList>,
                    response: Response<ResponseGetTodoList>
                ) {
                    if(response.isSuccessful) {
                        val id = sharedpref.geString(Constant.PREF_ID_ACTIVITY)
                        for (item in response.body()!!.data!!) {
                            if (item!!.id == id) {
                                todoactivity.setText(item.todo.toString())
                                dateCreated.setText(item.dateCreated.toString())
                                dateFinished.setText(item.dateFinished.toString())
                                if (item.todoStatus.toString() == "1") {
                                    todoStatus.setText("Sudah Selesai!")
                                    todoStatus.setEnabled(false)
                                    doneTodo.setEnabled(false)
                                    doneTodo.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY)
                                    updateTodo.setEnabled(false)
                                    updateTodo.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY)
                                    setReminder.setEnabled(false)
                                    setReminder.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY)
                                }
                                else todoStatus.setText("Belum Selesai")
                            }
                        }
                    }
                    else{
                        Toast.makeText(
                            applicationContext,
                            response.body()!!.data!!.get(0)!!.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "ERROR --> $t ",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }

            })
    }

    private fun setCompletedListener() {
        doneTodo.setOnClickListener {
            api.CompleteToDo("Todo.php", "updateTodo", id!!.toInt(), todo.toString(), 1)
                .enqueue(object : Callback<ResponseGetTodoList>{
                    override fun onResponse(
                        call: Call<ResponseGetTodoList>,
                        response: Response<ResponseGetTodoList>
                    ) {
                        if (response.isSuccessful){
                        Toast.makeText(applicationContext, "To Do Completed!" , Toast.LENGTH_SHORT).show()
                            sendNotification()
                            finish()
                        }
                        else{
                            Toast.makeText(applicationContext, "Gagal Mengupdate Data!", Toast.LENGTH_SHORT).show()
                            sendNotification()
                            finish()
                        }
                    }

                    override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Error --> $t ",
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }

                })
        }
    }
}