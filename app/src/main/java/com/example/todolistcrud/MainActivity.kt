package com.example.todolistcrud

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.api.NotificationRetrofit
import com.example.todolistcrud.firebase.NotificationData
import com.example.todolistcrud.firebase.PushNotification
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.Constant.Companion.PREF_TOKEN
import com.example.todolistcrud.helper.Constant.Companion.PREF_TYPE_FUNCTION
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.DataItem
import com.example.todolistcrud.model.ResponseGetTodoList
import com.example.todolistcrud.model.ResponseUser
import com.example.todolistcrud.model.SubmitTodo
import com.example.todolistcrud.user.UserActivity
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private lateinit var todoAdapter : TodoListAdapter
    private lateinit var todoList : RecyclerView
    private lateinit var setTextTodo : EditText
    private lateinit var addbtn : ImageButton
    private lateinit var username : TextView
    private lateinit var tipeList : TextView
    private lateinit var UserImage : ImageView
    lateinit var sharedpref: PreferencesHelper

    val TAG = "MainActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedpref = PreferencesHelper(this)

        FirebaseMessaging.getInstance().subscribeToTopic("global")

        setupTodoList()
        setupView()
        getTodayDate()
        setAddListener()
        settingUserListener()
    }

    private fun settingUserListener() {
        settingUser.setOnClickListener {
            var intent = Intent(this, UserActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun getTodayDate() {
        var todayDate : TextView = findViewById(R.id.dateShow)
        var cal = Calendar.getInstance()
        todayDate.text = "  " + SimpleDateFormat("EEE, dd-MM-yyyy").format(cal.time).toString()
    }

    private fun setAddListener() {
        addbtn.setOnClickListener {
            if (setTextTodo.text.toString().isNotEmpty()){
                val todo = setTextTodo.text
                api.addTodoList("Todo.php", "tambahTodo", setTextTodo.text.toString())
                    .enqueue(object : Callback<SubmitTodo>{
                        override fun onResponse(
                            call: Call<SubmitTodo>,
                            response: Response<SubmitTodo>
                        ) {
                            if (response.isSuccessful){
                                val pesan = response.body()
                                Toast.makeText(
                                    applicationContext,
                                    pesan!!.message,
                                    Toast.LENGTH_SHORT).show()
                                PushNotification(
                                    NotificationData("You have a New ToDo!", "New ToDo : $todo"),
                                    sharedpref.geString(PREF_TOKEN).toString()
                                ).also {
                                    sendNotification(it)
                                }
                                onStart()
                                setTextTodo.text.clear()
                            }
                            else{
                                val pesan = response.body()
                                Toast.makeText(
                                    applicationContext,
                                    pesan!!.message,
                                    Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Error!",
                                Toast.LENGTH_SHORT).show()
                        }

                    })
            }
            else{
                Toast.makeText(
                    applicationContext,
                    "ToDo tidak boleh kosong!",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun sendNotification(notification: PushNotification) = CoroutineScope(Dispatchers.IO).launch {
        try {
            NotificationRetrofit.Notifapi.postNotification(notification)
        } catch (e: Exception){
            Log.e(TAG, e.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        getTodoList()
    }

    private fun setupTodoList(){
        todoList = findViewById(R.id.todoList)
        todoAdapter = TodoListAdapter(arrayListOf(), object : TodoListAdapter.OnAdapterListener{
            override fun OnClick(dataTodo: DataItem) {
                sharedpref.put(Constant.PREF_ID_ACTIVITY, dataTodo.id.toString())
                startActivity(Intent(this@MainActivity, DetailActivity::class.java)
                    .putExtra("todo", dataTodo.todo)
                    .putExtra("status", dataTodo.todoStatus)
                    .putExtra("id", dataTodo.id)
                    .putExtra("created", dataTodo.dateCreated)
                    .putExtra("finish", dataTodo.dateFinished)
                )
            }

            override fun OnDelete(dataTodo: DataItem) {
                api.deleteTodoList("Todo.php", "deleteTodo", dataTodo.id!!.toInt())
                    .enqueue(object : Callback<SubmitTodo>{
                        override fun onResponse(
                            call: Call<SubmitTodo>,
                            response: Response<SubmitTodo>
                        ) {
                            if (response.isSuccessful){
                                Toast.makeText(
                                    applicationContext,
                                    response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                                onStart()
                            }
                            else{
                                Toast.makeText(
                                    applicationContext,
                                    response.body()!!.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<SubmitTodo>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Data tidak dapat dihapus!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    })
            }

        })
        todoList.adapter = todoAdapter
    }

    @SuppressLint("SetTextI18n")
    private fun setupView(){
        setTextTodo = findViewById(R.id.setTodo)
        addbtn = findViewById(R.id.addBtn)
        username = findViewById(R.id.username)
        tipeList = findViewById(R.id.tipeList)
        UserImage = findViewById(R.id.userProfile)

        Glide.with(this).load(sharedpref.geString(Constant.PREF_PHOTO_PROFILE)).into(UserImage)

        getUserName()

        when(sharedpref.geString(PREF_TYPE_FUNCTION)){
            "getTodoList" -> tipeList.text = "(All Task)"
            "getUncompleted" -> tipeList.text = "(Uncomplete Task)"
            "getCompleted" -> tipeList.text = "(Completed Task)"
            null -> tipeList.text = "(All Task)"
        }
    }

    private fun getUserName() {
        api.getUserIdentity("User.php", "ListUser")
            .enqueue(object : Callback<ResponseUser>{
                override fun onResponse(
                    call: Call<ResponseUser>,
                    response: Response<ResponseUser>
                ) {
                    for(item in response!!.body()!!.data!!){
                        if (item!!.id == sharedpref.geString(Constant.PREF_ID_USER)){
                            username.text = item.firstname.toString() + " " + item.lastname.toString()
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "ERROR!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
    }

    private fun setTodoList(responseGetTodoList: ResponseGetTodoList?){
        val data = responseGetTodoList?.data
        todoAdapter.setData(data as List<DataItem>)
    }

    private fun getTodoList(){
        api.getTodoList("Todo.php", sharedpref.geString(PREF_TYPE_FUNCTION).toString())
            .enqueue(object : Callback<ResponseGetTodoList>{
                override fun onResponse(
                    call: Call<ResponseGetTodoList>,
                    response: Response<ResponseGetTodoList>
                ) {
                    if (response.isSuccessful){
                        setTodoList(response.body())
                    }
                }
                override fun onFailure(call: Call<ResponseGetTodoList>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "To Do Not Found!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }
}