package com.example.todolistcrud.helper

class Constant {

    companion object{

        val PREF_IS_LOGIN = "PREF_IS_LOGIN"
        val PREF_EMAIL = "PREF_EMAIL"
        val PREF_PASSWORD = "PREF_PASSWORD"
        val PREF_ID_USER = "PREF_ID_USER"
        val PREF_USERNAME = "PREF_USERNAME"
        val PREF_PHOTO_PROFILE = "PREF_PHOTO_PROFILE"
        val PREF_TYPE_FUNCTION = "PREF_TYPE_FUNCTION"
        val PREF_TOKEN = "PREF_TOKEN"
        val PREF_ID_DEVICE = "PREF_ID_DEVICE"
        val PREF_ID_ACTIVITY = "PREF_ID_ACTIVITY"

        const val BASE_URL = "https://fcm.googleapis.com"
        const val SERVER_KEY = "AAAAp4x3cIg:APA91bF_CQnIuBbMDQlOW7z8iVFvjY6MVCkTJDGvTPiNh_dHmUANqksernFcoE-x0OroF5jrURsgwLbxjKpO5E2JUBhdDhVlSRWsdcW7JWzDN372ZoQj0lknFqlLZQecZ-kelW2Ir8te"
        const val CONTENT_TYPE = "application/json"

    }

}