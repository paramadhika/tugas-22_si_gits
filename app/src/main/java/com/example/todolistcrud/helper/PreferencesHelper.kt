package com.example.todolistcrud.helper

import android.content.SharedPreferences
import android.content.Context
import android.content.SharedPreferences.*

class PreferencesHelper (context: Context){

    private val PREFS_NAME = "sharedpregLoginUser"
    private var sharedpref : SharedPreferences
    val editor : Editor

    init {
        sharedpref = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        editor = sharedpref.edit()
    }

    fun put(key: String, value: String){
        editor.putString(key, value).apply()
    }

    fun geString(key: String) : String?{
        return sharedpref.getString(key, null)
    }

    fun put(key: String, value: Boolean){
        editor.putBoolean(key, value).apply()
    }

    fun getBoolean(key: String): Boolean{
        return sharedpref.getBoolean(key, false)
    }

    fun clear(){
        editor.clear()
            .apply()
    }
}