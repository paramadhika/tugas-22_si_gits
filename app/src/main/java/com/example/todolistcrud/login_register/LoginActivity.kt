package com.example.todolistcrud.login_register

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.EditText
import android.widget.Toast
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.todolistcrud.R
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.helper.Constant
import com.example.todolistcrud.helper.Constant.Companion.PREF_ID_DEVICE
import com.example.todolistcrud.helper.Constant.Companion.PREF_ID_USER
import com.example.todolistcrud.helper.Constant.Companion.PREF_TOKEN
import com.example.todolistcrud.helper.Constant.Companion.PREF_USERNAME
import com.example.todolistcrud.helper.PreferencesHelper
import com.example.todolistcrud.model.ResponseUser
import com.example.todolistcrud.user.UserActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor


class LoginActivity : AppCompatActivity() {

    lateinit var sharedpref : PreferencesHelper
    private val api by lazy { ApiRetrofit().endpoint }
    private lateinit var loginEmail : EditText
    private lateinit var loginPass : EditText
    private lateinit var idUser : String
    private lateinit var username : String

    private lateinit var deviceID : String

    private lateinit var executor : Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setLoginView()

        sharedpref = PreferencesHelper(this)

        setLoginListener()

        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback(){
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Toast.makeText(this@LoginActivity, "Authentication Error: $errString", Toast.LENGTH_SHORT).show()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                deviceID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
                setLoginAuthListener(deviceID)
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Toast.makeText(this@LoginActivity, "Authentication Failed", Toast.LENGTH_SHORT).show()
            }
        })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Fingerprint Authentication")
            .setSubtitle("Please use fingerprint to Login")
            .setNegativeButtonText("Use Email & Password instead")
            .build()

        LoginAuth.setOnClickListener {
            biometricPrompt.authenticate(promptInfo)
        }

        setSignupListener()
    }

    private fun setLoginAuthListener(deviceID: String) {
        api.loginAuth("User.php", "login_auth", deviceID)
            .enqueue(object : Callback<ResponseUser>{
                override fun onResponse(
                    call: Call<ResponseUser>,
                    response: Response<ResponseUser>
                ) {
                    if (response.isSuccessful){
                        for (item in response.body()!!.data!!){
                            idUser = item!!.id.toString()
                            username = item.firstname.toString() + " " + item.lastname.toString()
                        }
                        saveSessionLogin(loginEmail.text.toString(), loginPass.text.toString(), idUser, username, deviceID)
                        Toast.makeText(
                            applicationContext,
                            "BERHASIL LOGIN!",
                            Toast.LENGTH_SHORT
                        ).show()
                        retriveToken()
                        LoginIntent()
                    }
                }
                override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                    Toast.makeText(
                        applicationContext,
                        "This device have not bind to any account!",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
    }

    private fun setSignupListener() {
        signupfromlgn.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }


    private fun setLoginListener() {
        LoginBtn.setOnClickListener {
            if (loginEmail.text.isNotEmpty() && loginPass.text.isNotEmpty()){
                api.loginUser("User.php", "Login", loginEmail.text.toString(), loginPass.text.toString())
                    .enqueue(object : Callback<ResponseUser>{
                        override fun onResponse(
                            call: Call<ResponseUser>,
                            response: Response<ResponseUser>
                        ) {
                            if (response.isSuccessful){
                                for(item in response.body()!!.data!!){
                                    idUser = item!!.id.toString()
                                    username = item.firstname.toString() + " " + item.lastname.toString()
                                    if (item.device_id == "" || item.device_id == null){
                                        deviceID = ""
                                    }
                                    else deviceID = item.device_id
                                }
                                saveSessionLogin(loginEmail.text.toString(), loginPass.text.toString(), idUser, username, deviceID)
                                Toast.makeText(
                                    applicationContext,
                                    "BERHASIL LOGIN!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                retriveToken()
                                LoginIntent()
                            }
                            else{
                                Toast.makeText(
                                    applicationContext,
                                    "GAGAL LOGIN!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Email atau Password Salah!",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    })
            }
            else{
                Toast.makeText(
                    applicationContext,
                    "Masukkan Email & Password!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    @SuppressLint("StringFormatInvalid")
    private fun retriveToken() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->

            // Get new FCM registration token
            val token = task.result
            sharedpref.put(PREF_TOKEN, token.toString())

            //cek token
//            Toast.makeText(baseContext, "Token Saat ini : $token", Toast.LENGTH_LONG).show()
//            Log.d(TAG, "Token saat ini : $token")
        })
    }

    private fun saveSessionLogin(email: String, pass: String, id:String, userName: String, deviceID : String){
        sharedpref.put(Constant.PREF_EMAIL, email)
        sharedpref.put(Constant.PREF_PASSWORD, pass)
        sharedpref.put(Constant.PREF_IS_LOGIN, true)
        sharedpref.put(PREF_ID_USER, id)
        sharedpref.put(PREF_USERNAME, userName)
        sharedpref.put(PREF_ID_DEVICE, deviceID)
    }

    private fun setLoginView() {
        loginEmail = findViewById(R.id.LoginEmail)
        loginPass = findViewById(R.id.LoginPass)
    }

    override fun onStart() {
        super.onStart()
        LoginIntent()
    }

    private fun LoginIntent(){
        if(sharedpref.getBoolean(Constant.PREF_IS_LOGIN)){
            username = sharedpref.geString(PREF_USERNAME).toString()
            Toast.makeText(
                applicationContext,
                "Selamat Datang ${username}",
                Toast.LENGTH_SHORT
            ).show()
            idUser = sharedpref.geString(PREF_ID_USER).toString()
            val intent = Intent(this, UserActivity::class.java)
                .putExtra("idUser", idUser)
            startActivity(intent)
            finish()
        }
    }
}