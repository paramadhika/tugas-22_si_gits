package com.example.todolistcrud.login_register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.todolistcrud.R
import com.example.todolistcrud.api.ApiRetrofit
import com.example.todolistcrud.model.ResponseUser
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    private val api by lazy { ApiRetrofit().endpoint }
    private lateinit var inputFirstName : EditText
    private lateinit var inputLastName : EditText
    private lateinit var inputEmail : EditText
    private lateinit var inputPass : EditText
    private lateinit var signUpbtn : Button
    private lateinit var signInbtn : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        setUpView()
        onClick()
    }

    private fun onClick() {
        signUpbtn.setOnClickListener {

            if (inputFirstName.text.trim().toString() == "" ||
                inputLastName.text.trim().toString() == "" ||
                inputEmail.text.toString() == ""||
                inputPass.text.toString() == "")
                {
                    Toast.makeText(
                        this,
                        "Harap Lengkapi Data Registrasi!",
                        Toast.LENGTH_LONG).show()
                }
            else{
                api.regisNewUser("User.php", "daftarUserBaru",
                                inputFirstName.text.toString(),
                                inputLastName.text.toString(),
                                inputEmail.text.toString(),
                                inputPass.text.toString())
                    .enqueue(object : Callback<ResponseUser>{
                        override fun onResponse(
                            call: Call<ResponseUser>,
                            response: Response<ResponseUser>
                        ) {
                            Toast.makeText(
                                applicationContext,
                                "Berhasil Registrasi!\nMengalihkan ke halaman Login...",
                                Toast.LENGTH_LONG).show()
                            val intent = Intent(applicationContext, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        }

                        override fun onFailure(call: Call<ResponseUser>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Gagal Registrasi!",
                                Toast.LENGTH_LONG).show()
                        }

                    })
            }
        }

        SignInfromReg.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setUpView() {
        inputFirstName = findViewById(R.id.RegFirstName)
        inputLastName = findViewById(R.id.RegLastName)
        inputEmail = findViewById(R.id.RegEmail)
        inputPass = findViewById(R.id.RegPass)
        signUpbtn = findViewById(R.id.RegisterBtn)
        signInbtn = findViewById(R.id.SignInfromReg)
    }
}